
import React, { Component } from 'react';
import {
  ActivityIndicator, View, StyleSheet, StatusBar, Text,
  ScrollView, Dimensions, TouchableOpacity, Alert
} from 'react-native';
import Button from 'react-native-button';
import { NavigationActions, NavigationEvents } from 'react-navigation'
import { GlobalStyles } from './GlobalStyles'
// import { HeartRateMonitor } from 'checkoutmyworkout-heart-rate-monitor';
import HeartRateMonitor from './HeartRateMonitor'

export default class DeviceListScreen extends Component<{}> {
  state = {
    devicesAreFound: false,
    deviceMap: new Map(),
    deviceId: null,
    deviceName: null,
    hrm: new HeartRateMonitor(),
  }

  render() {
    StatusBar.setBarStyle('light-content', true);

    const { hrm, deviceMap } = this.state;

    let devicesJSX = [];
    for (var [key, device] of deviceMap) {
      devicesJSX.push(
        <View key={key} style={[styles.deviceListContainer]}>
          <TouchableOpacity key={key} onPress={ () => {this.configureDevice(device)} }>
            <Text
              key={key}
              style={styles.deviceListText}
            >{device.name}</Text>
          </TouchableOpacity>
        </View>
      );
    }    

    return (
      <View style={styles.container}>
        <NavigationEvents
          onWillFocus={payload => { 
            console.log('focused');
            StatusBar.setBarStyle('dark-content', true);
            hrm.startHeartRateDevicesScan( (device, error) => {
              if (error) {
                hrm.stopHeartRateDevicesScan(); 
                console.log(`error scanning for heart rate devices: ${JSON.stringify(error)}`)
                Alert.alert(
                  error.reason,
                  '',
                  [
                    {text: 'OK', onPress: () => this.close()},
                  ],
                  { cancelable: false }
                );    
              }
              else {
                this.setState({
                  deviceMap: deviceMap.set(device.id, device),
                  devicesAreFound: true,
                });
              }
            });                
          }}
        />

        <View style={styles.header}>
          <Text style={styles.headerText}>
            Choose a Heart Rate Device
          </Text>
        </View>
        {this.renderSpinner()}
        <ScrollView>
        {devicesJSX}
        </ScrollView>
        <View style={GlobalStyles.bottomBarContainer}>
          <Button style={[GlobalStyles.bottomBarButton]}
            onPress={ () => { this.close() } }>
            Close
          </Button>
        </View>
      </View>
    );
  }

  renderSpinner() {
    if (!this.state.devicesAreFound) {
      return (
        <View style={styles.spinnerView}>
          <ActivityIndicator size="large" color="#181937" />
          <Text style={styles.spinnerText}>Loading Bluetooth devices...</Text>
        </View>
      );
    }
  }

  configureDevice(device) {
    console.log('device: ', device);
    this.state.hrm.stopHeartRateDevicesScan(); 
    let navProps = { deviceId: device.id, deviceName: device.name };
    console.log(`DeviceListScreen.configureDevice: navProps=${JSON.stringify(navProps)}`);
    this.props.navigation.navigate('Home', navProps);
  }

  close() {
    this.state.hrm.stopHeartRateDevicesScan(); 
    this.props.navigation.navigate('Home');
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    paddingTop: 40,
    marginBottom: 10,
    width: '100%',
    backgroundColor: '#181937',
    minHeight: Dimensions.get('window').height * .13,
  },
  headerText: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 7,
    fontWeight: 'bold',
    color: 'white',
    width: '100%',
    textAlign: 'center',
    fontSize: Dimensions.get('window').height * .03,
  },
  titleText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 15,
    marginBottom: 15,
    minHeight: Dimensions.get('window').height * .05,
  },
  deviceListContainer: {
    padding: 10,
  },
  deviceListText: {
    color: '#181937',
    fontSize: Dimensions.get('window').height * .04,
    alignItems: 'flex-start',
    fontWeight: 'bold',
    textAlign: 'left',
    paddingTop: 5,
  },
  spinnerView: {
    paddingTop: Dimensions.get('window').height * .10,
  },
  spinnerText: {
    color: '#181937',
    fontWeight: 'bold',
    fontSize: Dimensions.get('window').height * .03,
    paddingTop: Dimensions.get('window').height * .02,
  },
});

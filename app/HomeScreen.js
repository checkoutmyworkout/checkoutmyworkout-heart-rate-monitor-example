import React, { Component } from 'react';
import { 
  AsyncStorage, StyleSheet, Text, View, Dimensions, Button, 
  StatusBar, Image, Animated 
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import RNButton from 'react-native-button';
// import { HeartRateMonitor } from 'checkoutmyworkout-heart-rate-monitor';
import HeartRateMonitor from './HeartRateMonitor'

type Props = {};
export default class App extends Component<Props> {
  state = {
    toggleText: 'Start',
    isDeviceActive: false,
    isRunning: false,
    heartRate: 0,
    bounceValue: new Animated.Value(1),
    hrm: new HeartRateMonitor(),
  };

  render() {

    const { deviceId, deviceName, heartRate, hrm, toggleText, isDeviceActive, isRunning } = this.state;
    const { navigation } = this.props;

    Animated.spring(
      this.state.bounceValue,
      {
        toValue: 1,
        bounciness: 5,
        speed: .2,
      }
    ).start();

    return (
      
      <View style={styles.container}>
        <NavigationEvents
          onWillFocus={payload => { 
            StatusBar.setBarStyle('dark-content', true);

            let deviceId = navigation.getParam('deviceId', null);
            let deviceName = navigation.getParam('deviceName', null);
            console.log(`HomeScreen.NavigationEvents.onWillFocus: deviceId=${deviceId}, deviceName=${deviceName}`);

            if (deviceId && deviceName) {
              console.log('deviceName: ', deviceName);
              this.setState({
                deviceId: deviceId,
                deviceName: deviceName,
                isDeviceActive: true,
                isRunning: false
              });
            }
          }}
        />

        <View>
          <Text 
            style={[
              styles.heartRateText, 
              heartRate == 0 ? {color: '#aaa'} : {color: 'white'}
            ]}
          >
            {heartRate == 0 ? '--' : heartRate}
          </Text>
          <Animated.Image
            source={require('./images/heart.png')}
            style={[
              styles.heartImage, 
              heartRate == 0 ? {opacity: 0.1} : {opacity: 1.0},
              {
                transform: [
                  {scale: this.state.bounceValue},
                ]
              }
            ]}
          />
        </View>
        <Text style={styles.deviceIdText}>
        {deviceId != null ? deviceName : 'no device configured'}
        </Text>
        <RNButton 
          style={styles.rnbutton} 
          styleDisabled={{color: '#445'}}
          onPress={() => { this.startStop() }}
          disabled={!isDeviceActive}
        >
          {toggleText}
        </RNButton>
        <RNButton 
          style={styles.rnbutton} 
          onPress={() => { this.props.navigation.navigate('DeviceList') } }
          disabled={isRunning}
          styleDisabled={{color: '#445'}}
          >
          Choose Device
        </RNButton>
        <View style={styles.button}>
          <RNButton
            style={styles.aboutButton} 
            onPress={ () => { this.props.navigation.navigate('About') } }
          >
            About
          </RNButton>        
        </View>
      </View>
    );
  }

  startStop() {
    const { deviceId, hrm, isDeviceActive, isRunning } = this.state;

    if (isDeviceActive && !isRunning) {
      hrm.startHeartRateMonitor(deviceId, (result) => {
        this.setState({
          bounceValue: new Animated.Value(1.1),
          heartRate:  result.heartRate,
          toggleText: 'Stop',
          isRunning: true
        });
      })
    }
    else if (isDeviceActive && isRunning) {
      hrm.stopHeartRateMonitor(deviceId)
      .then(result => {
        this.setState({
          heartRate:  0,
          toggleText: 'Start',
          isRunning: false
        });
      })
    }

  }

}

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    color: '#181937',
    width: '100%',
    height: '100%',
    paddingBottom: windowHeight * .10,
  },
  deviceIdText: {
    fontSize: windowHeight * .02,
    textAlign: 'center',
    margin: 10,
  },
  rnbutton: {
    fontSize: windowHeight * .03,
    color: '#fff',
    backgroundColor: '#181937',
    width: windowWidth * .80,
    borderWidth: 3,
    borderColor: '#181937',    
    borderRadius: 9,
    margin: 5,
    marginTop: windowHeight * .02,
    overflow: 'hidden',
    padding: 10,
  },
  aboutButton: {
    marginTop: windowHeight * .05,
    color: '#181937',
    fontWeight: 'bold',
    overflow: 'hidden',
    fontSize: windowHeight * .03,
    padding: 10,
  },
  heartImage: {
    width: windowWidth * .70,
    height: windowWidth * .70,    
  },
  heartRateText: {
    position: 'absolute',
    width: windowWidth * .70,
    top: windowWidth * .18,
    textAlign: 'center',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: Dimensions.get('window').height * .12,
    zIndex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
  },
});


import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
} from 'react-native';

export const GlobalStyles = StyleSheet.create({
  controlButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 15,
    marginBottom: Dimensions.get('window').height * 0.04,
    width: '100%',
  },

  bottomBarContainer: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    bottom: 0,
    right: 0,
    height: Dimensions.get('window').height * .1,
    width: '100%',
    backgroundColor: '#555',
  },

  bottomBarButton: {
    fontSize: Dimensions.get('window').height * .03,
    padding: 5,
    marginTop: Dimensions.get('window').height * .01,
    marginRight: 12,
    backgroundColor: '#555',
    color: '#ddd',
  },

});

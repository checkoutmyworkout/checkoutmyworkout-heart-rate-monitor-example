import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen';
import AboutScreen from './AboutScreen';
import DeviceListScreen from './DeviceListScreen';

export const RootNavigator = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    DeviceList: {
      screen: DeviceListScreen,
    },
    About: {
      screen: AboutScreen,
    },
  },
  {
    headerMode: 'none',
  }
);
  
class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (<RootNavigator />);
  }
}

export default App;

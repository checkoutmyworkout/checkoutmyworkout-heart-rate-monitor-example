import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
} from 'react-native';
import Button from 'react-native-button';
import { NavigationActions } from 'react-navigation'
import { GlobalStyles } from './GlobalStyles'

export default class AboutScreen extends Component {
    constructor(props) {
        super(props);

    }    

  render() {

    return (
      <View style={styles.container}>
        <Image
          source={require('./images/checkoutmyheart.png')}
          style={styles.logo}
        />
        <Text style={ styles.headerText }>
          Heart Monitor Example
        </Text>
        <Text style={ styles.versionText }>
          v0.0.1
        </Text>
        <Text style={ styles.copyrightText }>
          copyright (c) 2018, Brian T. Webb, Apache License Version 2.0
        </Text>
        <View style={GlobalStyles.bottomBarContainer}>
          <Button style={[GlobalStyles.bottomBarButton]} onPress={ () => { this.cancel() } } >Close</Button>
        </View>
      </View>
    );
  }

  launchSupport() {
    Communications.email(['support@mlml.rocks'],null,null,'Market Leader Mother Lode Support','Please enter your question here');
  }

  cancel() {
    this.props.navigation.dispatch(NavigationActions.back());
  }
}

const windowWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#181937',
    backgroundColor: '#F5FCFF',
  },
  logo: {
    width: windowWidth * .90,
    height: windowWidth * .90,
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 15,
    marginBottom: 5,
  },
  versionText: {
    fontSize: 18,
    marginBottom: 20,
  },
  copyrightText: {
    fontSize: 12,
    marginTop: 30,
  },
});
